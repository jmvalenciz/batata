BATATA
=======

Descripción
-----------

El proyecto pretende crear un invernadero automatizado apto para los hogares urbanos de la cuidad
de Medellín. Para esto se usará:

*La placa de Open Hardware NodeMCU la cual está basada en el chip ESP8266
*Un servidor brindado por la Universidad EAFIT que facilitara las comunicaciones cliente-invernadero
*Una aplicacion web que mostrara estadisticas sobre la plantacion, su ubicacion y la ubicacion de
las plantas adyacentes
