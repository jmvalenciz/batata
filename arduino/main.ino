#include <ESP8266WiFi.h>
#include "DHT"

//WIFI
string SSID="";
string pass="";
string token="";

		//PUERTOS

//SENSORES
#define s_luz
#define s_humedad_t
#define s_humedad_a
#define DHTPIN
#define DHTTYPE DHT11

//ACTUADORES
#define a_valvula
#define a_ventilador

//DHT11
DHT dht(DHTPIN,DHTTYPE);

//VARIABLES
float humedad,temp;
bool ventilador,valvula;

//CONSTANTES
#define TEMP_MIN
#define TEMP_MAX
#define HUMEDAD_MIN
#define HUMEDAD_MAX

void setupp(){
	Serial1.begin(115200);
	dht.begin();

	pinMode(s_luz,INPUT);
	pinMode(s_humedad_t,INPUT);
	pinmode(s_humedad_a,INPUT);
	pinMode(a_valvula,OUTPUT);
	pinMode(a_ventilador,OUTPUT);

	WiFi.begin(SSID,pass);
	Serial.println("Conectando");
	while(WiFi.status() != WL_CONNECTED){
			delay(500);
    		Serial.print(".");
	}
	Serial.println("\nConectado con exito!");
	Serial.print("IP: ");
	Serial.println(WiFi.localIP());

}

void loop(){
	
	humedad = dht.readHumidity();
	temp = dht.readTemperature();

	if(isnan(temperatura)){
		Serial.println("Error al leer la temperatura");
		return 1;
	}
	else if(isnan(humedad)){
		Serial.println("Error al leer la humedad");
		return 1;
	}
	Serial.print("Humedad: ");
	Serial.println(humedad);
	Serial.print("Temperatura: ");
	Serial.println(temp);
	
	if(temp>=TEMP_MAX){
		digitalWrite(a_ventilador,HIGH);	
	}
	else{
		digitalWrite(a_ventilador,LOW);
	}

	delay(2000);
}
